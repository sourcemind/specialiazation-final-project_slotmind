# Parking App

---

This web application is aimed at solving the parking queue problem at the Webb Fontaine Armenia office. The following is a rough documentation of the application structure.

---

## Entities

* `Employee` (`E`)
* `Car` (`C`)
* `RemoteController` (`RC`)
* `ParkingSlots` (`PS`)
  * State (`shortTerm`, `longTerm`)

---

## Relationships

* `RC` **1:1** `E`
* `E` **1:m** `C`
* `E` **1:1** `PS`

---

## Features

* `E` registers
* `E` logs in
* `E` registers in queue
* `E` can see `C` list
* `E` can see (free) `PS`
* `E` gets calendar event on WF account (**Microsoft Graph API**):
  * When a `PS` frees up
  * When they should exchange `RC`
* `E` can free `PS` temporarily, notifying next `n` `E`'s in queue
* Temporary `PS` can be booked for at least `x` amount of time
* `E` gets matched to exchange `RC` with other `E`
* `E`'s are informed the day prior to the exchange
* `E` logs receival, not hand-off
* `E` can only enqueue after hand-off
* `E` can view own reservation history **`[TBD]`**
* `E` can see his/her turn in the queue
* `E` can remove self from queue
* `E` is not removed from queue when handing-off temporarily AND `E`'s reservation is not extended for the duration of their absence
* `E` can add description for temporary hand-off
* `E`'s can see each other's floor, room, etc.
* There are three queues **`[TBD]`**
  * Main (two weeks)
  * Short term
  * Temporary (unexpected leaves, etc.)

---
